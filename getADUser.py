#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys
import ldap3
import logging
import yaml
#log_level = logging.DEBUG
log_level = logging.WARNING
logging.basicConfig(format='%(levelname)s: %(name)s - %(asctime)s %(message)s',datefmt='%m/%d/%Y %H:%M:%S',level=log_level)
#logging.basicConfig(filename='getADUser.log',format='%(levelname)s: %(asctime)s %(message)s',datefmt='%m/%d/%Y %I:%M:%S %p',level=log_level)


def read_config(file_path):
    try:
        with open(file_path, 'r') as f:
            return yaml.safe_load(f)
    except Exception as Error:
        logging.warning(f'* Error loading config file. Additional info: { Error }')

def loadfile(filename):
    ''' Функция для чтения текстовых файлов '''
    data = []
    try:
        f = open(filename)
        lines = f.readlines()
        f.close()
    except IOError:
        logging.warning(f'Error opening file: {filename}')
        sys.exit(1)
    for line in range(len(lines)):
        textline = lines[line].split(":")
        uid = textline[0].lower()\
                .replace('@gov.tatar.ru','').replace('@govtatar','')\
                .replace('govtatar\\','').replace('govtatar//','')\
                .replace('@tatar.ru','').replace('tatar\\','')
        pwd = textline[1].rstrip()
        data.append([uid, pwd])
    return data

def parseUID(credential):
    ''' Функция убирает лишние символы, оставляя лишь sAMAccountName '''


def ldapinit(uri, username, passwd):
    ''' Функция предназначена для подключения к серверу AD'''
    try:
        server = ldap3.Server(uri, get_info=ldap3.ALL)
        ad = ldap3.Connection(server, username, passwd, auto_bind=True)
    except (Exception, error):
        logging.warning('* Error connecting to LDAP: {}'.format(error))
    return ad

def get_ADUser(username):
    logging.info(f'get_ADUser {username}')
    ADConnection = ldapinit(LDAP_URI, LDAP_USER, LDAP_PASSWD)
    searchFilter = '(sAMAccountName={})'.format(username)
    retrieveAttributes = LDAP_ATTRS
    try:
        ADConnection.search(BASE_DN, searchFilter, attributes=retrieveAttributes)
        User = ADConnection.entries
    except:
        User = False
        logging.warning(f'{User}')
    ADConnection.unbind()
    return User

def extract_user_data(userobject):
    user_info = []
    for attr in LDAP_ATTRS:
        try:
            user_info.append(userobject[0][attr][0])
        except IndexError:
            user_info.append("not defined")
    UserRecord = dict(zip(LDAP_ATTRS, user_info))
    return UserRecord

def main():
    if len(sys.argv) < 2:
        logging.warning('USAGE: {} process <filename>'.format(sys.argv[0]))
        logging.warning('or: {} show <sAMAccountName>'.format(sys.argv[0]))
        return False
    operation = sys.argv[1]
    if operation == 'process':
        UserTable = []
        allusers = loadfile(sys.argv[2])
        for user in allusers:
            U = get_ADUser(user[0])
            logging.info(f'get_ADUser returned: {U}')
            if U:
                UserData = extract_user_data(U)
                UserTable.append(UserData)
        with open('usertable.csv', mode='w') as output:
            for record in UserTable:
                print(f"{ record[LDAP_ATTRS[0]] }, { record[LDAP_ATTRS[1]] }",file=output)
        logging.info('Process complete!')
        return True
    elif operation == 'show':
        user_name = sys.argv[2]
        U = get_ADUser(user_name)
        if U:
            UserData = extract_user_data(U)
            print(f"{ UserData[LDAP_ATTRS[0]] }, { UserData[LDAP_ATTRS[1]] }")
        logging.info('show completed!')
        return True
    else:
        logging.info(f'* Operation { operation } not supported! Use: "process" or "show"')
    return True


# Loading configuration from yaml, and initializing runtime constants
CONFIG = 'config.yaml'
conf = read_config(CONFIG)
LDAP_URI = conf['identity_source']['ldap']['uri']
LDAP_USER = conf['identity_source']['ldap']['user']
LDAP_PASSWD = conf['identity_source']['ldap']['pw']
LDAP_ATTRS = conf['identity_source']['ldap']['ldapAttributes']
BASE_DN = conf['identity_source']["ldap"]['BaseDN']

if __name__ == "__main__":
    main()
    logging.info('getADUser call completed!')

