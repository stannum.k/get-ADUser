# getADUser - get user info from AD
This script is usefull for quickly obtain informtaion about known user:
* Get users DisplayName attribute
* Get users mail attribute
* Check if user password is valid

Configuration file is ./config.yaml.example, rename it to ./config.yaml before first run.
